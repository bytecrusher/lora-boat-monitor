**20210616 firmware_V1.01.bmb {- (critical) -}**

* Add page for device info, LoRa info and sensor values
* WLAN default settings (SSID: NoWa, Pass: 12345678, IP: 192.168.4.1)

{- Attention! After update changes all settings to default values. Please renew the settings. -}


**20210530 firmware_V1.00.bmb {+ (old) +}**

* Initial firmware with limited functionality (only settings, no view for measuring results)
* WLAN default settings (SSID: NoWa, Pass: 12345678, IP: 192.168.4.1)
